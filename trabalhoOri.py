import sys

import nltk

def filterList(x):
    aux = x[0]
    newList = []
    newList.append(x[0])
    specialCharactes = [' ', '\t' , '\n' , '?' , '.' , ',' , '!', ':' , '`','(',')','-','\'\'','``']
    for i in range(1, x.__len__()): #percorre toda a lista
        if x[i] in specialCharactes: #verifica se o elemento atual da lista está entre os separadores
            if aux not in specialCharactes: #verifica se o último elemento não está entre os separadores
                newList.append(' ') # se não estiver adiciona a nova lista
        else:
            newList.append(x[i]) # caso o elemento não seja um separador, ele é adicionado a lista
        aux = x[i]
    return "".join(newList)

def cleanList(x):
	newlist = []
	for i in range(0,x.__len__()):
		newlist.append (filterList(list(x[i])).lower())
	return newlist


def readOutputFile(path):

    file = open(path,'r')
    lst = file.readlines()
    newDictionary = {}
    for indice in lst:
        indice = indice.replace(':','')
        indice = indice.replace('\n','')
        indice = indice.split(' ')
        if newDictionary.get(indice[0]) == None:
            newDictionary[indice[0]] = {}
        for values in range(1,len(indice)):
            nro = indice[values].split(',')
            newDictionary[indice[0]][nro[0]] = nro[1]

    return newDictionary







def removeRadical(list_words,stemmer):
	list = []
	for i in list_words:
		list.append(stemmer.stem(i))
	return list

def tstDict(dictionary, pharse,i):
	for word in pharse:
		word = word.lower()
		if dictionary.get(word) == None:
			dictionary[word] = {}
			dictionary[word][i+1] = 0
		else:
			if dictionary.get(word).get(i+1) == None :
				dictionary[word][i+1] = 0
		dictionary[word][i+1] +=1
def findWord(dictionary, word, files):
    type_search = True # usado para diferenciar a buscas por palavra normais(True) por palavras com negação (False)
    if word[0]=='!':
        word = word[1:len(word)+1]
        # print(word + ' Entrou aqui ')
        type_search = False
    result = []
    files_index = [] # insere os index's dos arquivos
    files_with_word = [] # arquivos que contém a palavra
    word = word.lower()
    for i in range(0,len(files)):
        files_index.append(i+1)

    if dictionary.get(word) == None: # verifica se a palavra existe no dicionario
        if type_search: #verifica que a busca deve ser feita sem o operador de negação
            return [] #retorna uma lista vazia, pois não achou a palavra
        else: #verifica que a consulta usa not então busca todos os arquivos da base
            return files_index # retorna todos os arquivos da base
    else: #a palvra existe na base de dados
        for i in dictionary[word]: #recupera todas as ocorrencia da palavra na base
            if i not in result:
                files_with_word.append(i)
        if not type_search: #verifica que a consulta usa not, da pra otimizar essa parte fazendo a diferença entre todos os arquivos que estão na base pelos arquivos em que a palavra aparece
            return list(filter(lambda x: x not in files_with_word, files_index)) #retornar a diferença entre os arquivos da base e o total de arquivos
        else:
            return files_with_word

def getPriority(operand):
    if operand == '|':
        return 0
    elif operand == '&':
        return 1

def orOperator(list1, list2):
    result = list1
    result = result + list(filter(lambda x:x not in result,list2 ))
    # print(result)
    result.sort()
    return result

def andOperator(list1,list2):
    result = list(filter(lambda x:x in list2,list1))
    result.sort()
    return result

# nomeArq = sys.argv[1] //Primeiro parametro do terminal
arq = open(sys.argv[1], 'r', encoding='utf-8')
queryFile = open (sys.argv[2],'r', encoding='utf-8')
stopwords = nltk.corpus.stopwords.words("portuguese")
# print(stopwor,,ds)
conteudo = arq.readlines()
stemmer = nltk.stem.RSLPStemmer()
files = []
indiceInvertido = {}
query = queryFile.readlines()[0].replace('\n','')
for i in range(0,len(conteudo)):#percorrer o arquivo base
    filesrows = open(conteudo[i].replace('\n',''))#remove \n
    files.append(conteudo[i].replace('\n',''))
    temp_words = cleanList(filesrows.readlines())
	# print(temp_words)#lê as linhas de cada arquivo referenciado no arquivo base, logo em remove as pontuações e espaços duplicados
    # print(aux)
    palavras =nltk.word_tokenize( "".join(temp_words))
    # print(aux)
    aux = list(filter(lambda x: x not in stopwords, palavras))  # remove as stopwords
    # print(removeRadical(aux, stemmer) )
    tstDict(indiceInvertido, removeRadical(aux, stemmer), i)
    # print('-----------------------------------------')
#escreve no arquivo
# print(indiceInvertido)
# print(stopwords)
indices = open('saida\\indice.txt','w')
for key in indiceInvertido:
    # print(key, end=':')
    indices.write(key + ':')
    for key2 in indiceInvertido[key]:
        # print(' ' + str(key2) + ',' + str(indiceInvertido[key][key2]), end='')
        indices.write(' ' + str(key2) + ',' + str(indiceInvertido[key][key2]))
    # print('')
    indices.write('\n')
indices.close()


# auxF = findWord(indiceInvertido,'manifest',  True, files)
# print(auxF)
# print(indiceInvertido[removeRadical('Internet',stemmer)][1])

operand = []
operator = []
operand1 = ''
operand2 = ''

# query = 'cultural | televisão & !grupo & dar'
# query = query.replace('!','! ')
# print(query)
query = removeRadical(query.split(' '), stemmer)
print(query)

for i in range(0, len(query)):
    if ((query[i][0]=='!') & (query[i][1:-1].isalpha())) | (query[i].isalpha()):
        if (len(operator) > 0):
            if (operator[-1] == '&'):
                operand1 = operand.pop()
                operand2 = findWord(indiceInvertido,query[i] , files)
                # print('Operando 1: ', end=' ')
                print(operand1, end=' ')
                print(operator.pop(), end=' ')
                # print('Operando 2: ', end=' ')
                print(operand2)
                aux = andOperator(operand1, operand2)
                print('result:', end=' ')
                print(aux)
                operand.append(aux)

            else:
                operand.append(findWord(indiceInvertido, query[i], files))
        else:
            operand.append(findWord(indiceInvertido, query[i], files))
    else:
        operator.append(query[i])

while ((len(operator)>0) & (len(operand)>=2)):
    operand1 = operand.pop()
    operand2 = operand.pop()
    # print('Operando 1: ', end=' ')
    print(operand1, end=' ')
    print(operator.pop(), end= ' ')
    # print('Operando 2: ', end=' ')
    print(operand2)
    aux = orOperator(operand1, operand2)
    print('result:', end=' ')
    print(aux)
    # print('aux')
    # print(aux)
    operand.append(aux)

# print()
print('------------Resposta-----------------')
print(len(aux))
for i in range(0,len(aux)):
    print(files[aux[i]])

queryFile.close()
arq.close()

answer = open('resposta.txt','w')
answer.write(str(len(aux)) + '\n')
for i in range(0,len(aux)):
    answer.write(str(files[aux[i]])+'\n')
# print(operator)
answer.close()

# print(operand)
