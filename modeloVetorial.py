import sys
import nltk
import math

def clearList(x):#recebe uma lista de palavra. Remove todas pontuações definidas na função filterList
	newlist = []
	for i in range(0,len(x)):
		newlist.append (filterList(list(x[i])).lower())
	return newlist

def removeRadical(list_words,stemmer): #remove todos os radicais de uma lista de palavras
	list_result = []
	for i in list_words:
		list_result.append(stemmer.stem(i).strip())
	return list_result


def filterList(x):
    aux = x[0]
    newList = []
    newList.append(x[0])
    specialCharactes = [' ', '\t' , '\n' , '?' , '.' , ',' , '!', ':' , '`','(',')','-','\'\'','``']
    for i in range(1, len(x)): #percorre toda a lista
        if x[i] in specialCharactes: #verifica se o elemento atual da lista está entre os separadores
            if aux not in specialCharactes: #verifica se o último elemento não está entre os separadores
                newList.append(' ') # se não estiver adiciona a nova lista
        else:
            newList.append(x[i]) # caso o elemento não seja um separador, ele é adicionado a lista
        aux = x[i]
    return "".join(newList)

def createIndex(dictionary, pharse,i): #cria o indice Invertido, recebe um dicionario, uma lista de palavra e o indice do arquivo que está sendo lido
	for word in pharse:
		word = word.lower()
		if dictionary.get(word) == None:
			dictionary[word] = {}
			dictionary[word][i+1] = 0
		else:
			if dictionary.get(word).get(i+1) == None :
				dictionary[word][i+1] = 0
		dictionary[word][i+1] +=1


def escrevePesosArquivos(listaPesos,listaArquivos):
    pesos = open('pesos.txt','w')
    for indiceArq in range(0,len(listaPesos)) :
        pesos.write(listaArquivos[indiceArq] + ':')
        for indiceTermo in listaPesos[indiceArq]:
            if listaPesos[indiceArq][indiceTermo] != 0:
                pesos.write(' ' + str(indiceTermo) + ','+ str(listaPesos[indiceArq][indiceTermo]))
        pesos.write('\n')

vocabulario = []
arquivoBase = open(sys.argv[1], 'r', encoding='utf-8') #arquivo com os caminhos da base
arquivoConsulta = open (sys.argv[2],'r', encoding='utf-8')#arquivo com a consulta
stopwords = nltk.corpus.stopwords.words("portuguese")
conteudo = arquivoBase.readlines()
stemmer = nltk.stem.RSLPStemmer()
files =  []
indiceInvertido = {}
for i in range(0,len(conteudo)):#percorrer o arquivo base
    filesrows = open(conteudo[i].replace('\n',''))#Abre cada dos arquivos da base
    files.append(conteudo[i].replace('\n',''))
    temp_words = clearList(filesrows.readlines())
    palavras =nltk.word_tokenize( "".join(temp_words))
    aux = list(filter(lambda x: x not in stopwords, palavras))  # remove as stopwords
    createIndex(indiceInvertido, removeRadical(aux, stemmer), i)
listaOrdenada = list(indiceInvertido.keys())
vocabulario = list(indiceInvertido.keys())
vocabulario.sort()
listaPesos = []
tf = 0
idf= 0
ni = 0
tf_idf = 0
print(vocabulario)
for indiceTermo in range(0,len(vocabulario)):
    print(indiceTermo)
    ni = len(indiceInvertido[vocabulario[indiceTermo]])
    for i in range(0,len(files)):
        if i >= len(listaPesos):
            listaPesos.append({})
        if listaPesos[i].get(indiceTermo+1) == None: #Verifica que na posição i não existe o termo
                # print('Termo: ' + vocabulario[indiceTermo])
                # print('Arquivo: ')
                if indiceInvertido[vocabulario[indiceTermo]].get(i+1) !=None:
                    # tf = 1+ math.log10(indiceInvertido[vocabulario[indiceTermo]][i+1])
                    # idf =math.log10(len(files)/ni)
                    # tf_idf =tf* idf
                    tf_idf = (1+ math.log10(indiceInvertido[vocabulario[indiceTermo]][i+1])) * (math.log10(len(files)/ni))
                    # print(indiceInvertido[vocabulario[indiceTermo]][i+1])
                    # listaPesos[i][vocabulario[indiceTermo]] = indiceInvertido[vocabulario[indiceTermo]][i+1]
                    listaPesos[i][indiceTermo+1] =tf_idf
                else:
                    listaPesos[i][indiceTermo+1] = 0
print('--------------')
# print(listaPesos)
# print(indiceInvertido)
escrevePesosArquivos(listaPesos,files)
for i in range(0,len(vocabulario)):
    print(str(i+1))
print(vocabulario[i])
# consulta = "".join(arquivoConsulta.readlines()).strip().replace('\n
# print(consulta)
consulta = arquivoConsulta.readline().strip().replace('\n','')
subconsultas = removeRadical(consulta.split('|'), stemmer)
print(subconsultas)
     
